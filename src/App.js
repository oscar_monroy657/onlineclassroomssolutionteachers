import React, { useState, useEffect, useContext } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
// Theme
import { MuiThemeProvider } from '@material-ui/core';
import theme from './theme/theme';
// Context Firebase
import { FirebaseContext } from './server'
// Session Context
import { useSessionStateValue } from './session/SessionContext';
// Components
import HomePage from './pages/homePage/HomePage';
import { MainSpinner } from './components/spinner'
import LoginPageProfessors from './pages/loginPageProffesors/LoginPageProfessors';
import CreateAccount from './pages/createAccountPage/CreateAccount'
import SnackBar from './components/snackBar/SnackBar'
import RecoverPassword from './pages/recoverPasswordPage/RecoverPassword'
import AuthenticatedRoute from './auth/AuthenticatedRoute'
import DashboardTeacher from './pages/dashboardTeacherPage/DashboardTeacher';
import LoginHeadmaster from './pages/loginHeadmasterPage/LoginHeadmaster';
import CreateHeadmasterAccount from './pages/createHeadmasterAccount/CreateHeadmasterAccount';
import HeadmasterDashboard from './pages/headmasterDashboardPage/HeadmasterDashboard';

const App = () => {
  let firebase = useContext(FirebaseContext)
  const [isFirebaseReady, setIsFirebaseReady] = useState(false)
  const [{openSnackBar}, dispatch] = useSessionStateValue()

  useEffect(() => {
    firebase.isReady().then(val => setIsFirebaseReady(val))
  }, [firebase])

  return isFirebaseReady !== false ? (
    <MuiThemeProvider theme={theme}>
      <Router>
        <SnackBar openSnackBar={openSnackBar} dispatch={dispatch} />
        <Switch>
          <Route exact path='/' component={HomePage}/>
          <Route exact path='/login-teachers' component={LoginPageProfessors}/>
          <Route exact path='/login-headmasters' component={LoginHeadmaster}/>
          <Route exact path='/register-teacher' component={CreateAccount}/>
          <Route exact path='/register-headmaster' component={CreateHeadmasterAccount}/>
          <Route eact path='/recover-password' component={RecoverPassword}/>
          <AuthenticatedRoute
            exact
            path="/dashboard-teacher"
            component={DashboardTeacher}
            authFirebase={firebase.auth.currentUser}
          />
          <AuthenticatedRoute
            exact
            path="/dashboard-headmaster"
            component={HeadmasterDashboard}
            authFirebase={firebase.auth.currentUser}
          />
        </Switch>
      </Router>
    </MuiThemeProvider>
  )
  : <MainSpinner/> ;
}
// /dashboard-teacher
// dashboard-headmaster
export default App;
