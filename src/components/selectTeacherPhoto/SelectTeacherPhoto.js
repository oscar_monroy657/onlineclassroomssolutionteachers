import React from "react";
import ImageUploader from "react-images-upload";
import { v4 as uuidv4 } from 'uuid'

const SelectTeacherPhoto = ({ setUserPhoto }) => {
  let imageComponentKey = uuidv4()

  const onChangeSelectProfileImage = photos => {
    const photo = photos[0]
    const photoURL = URL.createObjectURL(photo)

    const photoData = {
      url: photoURL,
      name: photo.name,
      data: photos
    }
    setUserPhoto(photoData)
  }

  return (
    <ImageUploader
      withIcon={false}
      key={imageComponentKey}
      singleImage={true}
      buttonText="Selecciona una foto de perfil"
      onChange={onChangeSelectProfileImage}
      imgExtension={[".jpg", ".png", ".gif", ".jpeg"]}
      maxFileSize={5242880}
    />
  );
};

export default SelectTeacherPhoto;
