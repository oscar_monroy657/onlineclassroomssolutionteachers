import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
// Firebase Context
import FirebaseHelper, { FirebaseContext } from './server'
// Session Context
import { mainReducer } from './session/reducers'
import { SessionStateProvider } from './session/SessionContext'

const initialState = {}

ReactDOM.render(
  <FirebaseContext.Provider value={new FirebaseHelper()}>
    <SessionStateProvider initialState={initialState} reducer={mainReducer}>
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </SessionStateProvider>
  </FirebaseContext.Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
