import React, { useState, useEffect } from 'react';
import { Grid, Container, Button, Paper, TextField, Avatar, CircularProgress } from "@material-ui/core";
import { useStyles } from "../../styles/createAccountStyles";
import CircleComponents from "../../components/circles/CircleComponents";
import { firebaseConsumer } from '../../server';
import initialUserData from '../../utils/initialUserState'
import SelectTeacherPhoto from '../../components/selectTeacherPhoto/SelectTeacherPhoto';
import { useSessionStateValue } from '../../session/SessionContext'
import displaySnackBar from '../../session/actions/snackBarActions'
import { createUserAction } from '../../session/actions/sessionActions';
import FormValidate, { convertStringToArray } from './formValidate';
import createSchoolLocalKeys from '../../utils/createSchoolLocalKeys';
import { SelectSchoolComponent } from '../../components/selectComponent/SelectComponent';

const CreateAccount = props => {
  const classes = useStyles();
  const { firebase, history } = props
  const [ {session}, dispatch ] = useSessionStateValue()

  const [userData, setUserData] = useState(initialUserData)
  const [userPhoto, setUserPhoto] = useState({})
  const [userPassword, setUserPassword] = useState('')

  const [schoolsData, setSchoolsData] = useState([])
  const [schoolSelected, setSchoolSelected] = useState({})
  const [isFirstTime, setIsFirstTime] = useState(true)
  const [isLoading, setIsLoading] = useState(false)
  const [subjects, setSubjects] = useState('')

  useEffect(() => {
    if(isFirstTime) {
      firebase.getPublicDataFromSchools().then(data => {
        const schoolArray = data.docs.map(doc => {
          const data = doc.data()
          const id = doc.id

          return {id, ...data}
        })
        setSchoolsData(schoolArray)
      })
      .catch(error => console.log(error))
      setIsFirstTime(false)
    }
  }, [isFirstTime, firebase])

  const handleOnChange = e => {
    setUserData({
      ...userData,
      [e.target.name] : e.target.value
    })
  }

  const handleOnSubmit = async e => {
    e.preventDefault()

    const formIsCorrectly = FormValidate(userData, userPhoto, userPassword, subjects)

    if(formIsCorrectly.isCorrectly === false) {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: formIsCorrectly.message
      })
      return
    }
    setIsLoading(true)
    convertStringToArray(subjects, userData)
    createSchoolLocalKeys(schoolSelected)

    const result = await createUserAction(dispatch, firebase, schoolSelected, userPassword, userPhoto, userData)

    if(result.status){
      history.push('/dashboard-teacher')
    }else {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: result.message
      })
      setIsLoading(false)
    }
    
  }
  return (
    <Container>
      <CircleComponents/>
      <Grid
        container
        spacing={2}
        className={classes.container}
        alignItems="center"
        justify="center"
      >
        <Grid item xs={12} sm={6} md={5} className={classes.itemContainer}>
          <Paper className={classes.paper}>
            <form className={classes.form} onSubmit={handleOnSubmit}>
              <Avatar className={classes.avatar} src={userPhoto.url}/>
              <SelectTeacherPhoto setUserPhoto={setUserPhoto}/>
              <TextField
                variant="outlined"
                label="Nombres"
                name="userName"
                value={userData.userName}
                fullWidth
                margin="normal"
                onChange={handleOnChange}
              />
              <TextField
                variant="outlined"
                label="Apellidos"
                name="userLastName"
                value={userData.userLastName}
                fullWidth
                margin="normal"
                onChange={handleOnChange}
              />
              <SelectSchoolComponent
                arrayData={schoolsData}
                labelText="Establecimiento"
                selectValue={userData.userSchool}
                changeUserSchool={handleOnChange}
                changeValueState={setSchoolSelected}
              />
              <TextField
                variant="outlined"
                label="Materias"
                name="userSubject"
                value={subjects}
                fullWidth
                margin="normal"
                onChange={e => setSubjects(e.target.value)}
              />
              <TextField
                type="email"
                variant="outlined"
                label="Email"
                name="userEmail"
                value={userData.userEmail}
                fullWidth
                margin="normal"
                onChange={handleOnChange}
                autoComplete="username"
              />
              <TextField
                variant="outlined"
                type="password"
                label="Contraseña"
                name="userPassword"
                value={userPassword}
                onChange={e => setUserPassword(e.target.value)}
                fullWidth
                autoComplete="current-password"
              />
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  fullWidth
                >
                  {isLoading ? <CircularProgress color="inherit" size={30}/> : 'Crear cuenta' }
                </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}
 
export default firebaseConsumer(CreateAccount);