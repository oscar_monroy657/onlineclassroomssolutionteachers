const FormValidate = (userData, userPhoto, userPassword, subjects) => {
  let isCorrectly = true
  let message = ''

  if(userData.userName === '' || userData.userLastName === '' || userData.userEmail === '' || userPassword === '' || userData.userSchool === '' || subjects === '') {
    isCorrectly = false
    message = 'Todos los campos son obligatorios'

    return {isCorrectly, message}
  }
  if(!userPhoto.name) {
    isCorrectly = false
    message = 'Por favor selecciona una foto de perfil'

    return {isCorrectly, message}
  }

  return {isCorrectly, message}
}

export const convertStringToArray = (subjectsString, userData) => {
  let arraySubjects = []

  if(subjectsString.includes(',')) {
    arraySubjects = subjectsString.split(',')
    userData.userSubjects = arraySubjects
  }else {
    arraySubjects = subjectsString.split()
    userData.userSubjects = arraySubjects
  }
}

export default FormValidate