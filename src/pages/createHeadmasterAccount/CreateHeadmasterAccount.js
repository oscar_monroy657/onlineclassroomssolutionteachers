import React, { useState } from 'react';
import { Grid, Container, Button, Paper, TextField, Avatar, CircularProgress } from "@material-ui/core";
import { useStyles } from "../../styles/createAccountStyles";
import CircleComponents from "../../components/circles/CircleComponents";
import { firebaseConsumer } from '../../server';
import initialUserData from '../../utils/initialUserState'
import SelectTeacherPhoto from '../../components/selectTeacherPhoto/SelectTeacherPhoto';
import { useSessionStateValue } from '../../session/SessionContext'
import displaySnackBar from '../../session/actions/snackBarActions'
import { createHeadmasterAction } from '../../session/actions/sessionActions';
import FormValidate from './formValidate';

const CreateHeadmasterAccount = props => {
  const classes = useStyles();
  const { firebase, history } = props
  const [ {session}, dispatch ] = useSessionStateValue()

  const [userData, setUserData] = useState(initialUserData)
  const [userPhoto, setUserPhoto] = useState({})
  const [userPassword, setUserPassword] = useState('')

  const [isLoading, setIsLoading] = useState(false)

  const handleOnChange = e => {
    setUserData({
      ...userData,
      [e.target.name] : e.target.value
    })
  }

  const handleOnSubmit = async e => {
    e.preventDefault()

    const formIsCorrectly = FormValidate(userData, userPhoto, userPassword)

    if(formIsCorrectly.isCorrectly === false) {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: formIsCorrectly.message
      })
      return
    }
    setIsLoading(true)

    const result = await createHeadmasterAction(dispatch, firebase, userPassword, userPhoto, userData)

    if(result.status){
      history.push('/dashboard-headmaster')
    }else {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: result.message
      })
      setIsLoading(false)
    }
    
  }
  return (
    <Container>
      <CircleComponents/>
      <Grid
        container
        spacing={2}
        className={classes.container}
        alignItems="center"
        justify="center"
      >
        <Grid item xs={12} sm={6} md={5} className={classes.itemContainer}>
          <Paper className={classes.paper}>
            <form className={classes.form} onSubmit={handleOnSubmit}>
              <Avatar className={classes.avatar} src={userPhoto.url}/>
              <SelectTeacherPhoto setUserPhoto={setUserPhoto}/>
              <TextField
                variant="outlined"
                label="Nombres"
                name="userName"
                value={userData.userName}
                fullWidth
                margin="normal"
                onChange={handleOnChange}
              />
              <TextField
                variant="outlined"
                label="Apellidos"
                name="userLastName"
                value={userData.userLastName}
                fullWidth
                margin="normal"
                onChange={handleOnChange}
              />
              <TextField
                type="email"
                variant="outlined"
                label="Email"
                name="userEmail"
                value={userData.userEmail}
                fullWidth
                margin="normal"
                onChange={handleOnChange}
                autoComplete="username"
              />
              <TextField
                variant="outlined"
                type="password"
                label="Contraseña"
                name="userPassword"
                value={userPassword}
                onChange={e => setUserPassword(e.target.value)}
                fullWidth
                autoComplete="current-password"
              />
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  fullWidth
                >
                  {isLoading ? <CircularProgress color="inherit" size={30}/> : 'Crear cuenta' }
                </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}
 
export default firebaseConsumer(CreateHeadmasterAccount);