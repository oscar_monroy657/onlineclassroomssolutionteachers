import React, {useEffect} from 'react';
import { Typography, Container, Button } from '@material-ui/core';
import { firebaseConsumer } from '../../server';
import { useSessionStateValue } from '../../session/SessionContext';
import { signOutAction } from '../../session/actions/sessionActions';

const UserDashboard = ({firebase, history}) => {
  const [{session}, dispatch] = useSessionStateValue()

  useEffect(() => {
    const schoolKeys = JSON.parse(localStorage.getItem('schoolKeys'))
    if(firebase.auth.currentUser !== null && !session) {
      firebase
      .getTeacherDataFromFirestore(schoolKeys.headmasterId, schoolKeys.schoolId, firebase.auth.currentUser.uid)
      .then(student => {
        firebase.getSchoolFromFirestore(schoolKeys.headmasterId, schoolKeys.schoolId)
        .then(school => {
          dispatch({
            type: 'REGISTER_USER',
            user: student.data(),
            school: school.data()
          })
        })
      })
    }
  }, [firebase, dispatch, session])

  const signOutClick = () => {
    signOutAction(dispatch, firebase)
    .then(success => {
      history.push('/')
    })
    .catch(error => console.log(error))
  }

  return (
    <Container>
      <Typography component="h1" variant="h3">Bienvenido a tu Dashboard</Typography>
      <Button variant="contained" color="primary" onClick={signOutClick}>Cerrar sesión</Button>
    </Container>
  );
}
 
export default firebaseConsumer(UserDashboard);