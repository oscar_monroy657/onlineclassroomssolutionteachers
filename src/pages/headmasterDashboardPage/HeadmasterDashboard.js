import React, {useEffect} from 'react';
import { Typography, Container, Button } from '@material-ui/core';
import { firebaseConsumer } from '../../server';
import { useSessionStateValue } from '../../session/SessionContext';
import { signOutAction } from '../../session/actions/sessionActions';

const HeadmasterDashboard = ({firebase, history}) => {
  const [{session}, dispatch] = useSessionStateValue()

  useEffect(() => {
    if(firebase.auth.currentUser !== null && !session) {
      firebase
      .getHeadmasterDataFromFirestore(firebase.auth.currentUser.uid)
      .then(doc => {
        dispatch({
          type: 'REGISTER_USER',
          user: doc.data(),
          school: {}
        })
      })
    }
  }, [firebase, dispatch, session])

  const signOutClick = () => {
    signOutAction(dispatch, firebase)
    .then(success => {
      history.push('/')
    })
    .catch(error => console.log(error))
  }

  return (
    <Container>
      <Typography component="h1" variant="h3">Bienvenido a tu HeadMasterDashboard</Typography>
      <Button variant="contained" color="primary" onClick={signOutClick}>Cerrar sesión</Button>
    </Container>
  );
}
 
export default firebaseConsumer(HeadmasterDashboard);