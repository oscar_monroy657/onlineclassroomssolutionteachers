import React from "react";
import { Grid, Container, Typography, Button } from "@material-ui/core";
import { Link } from 'react-router-dom'
import { useStyles } from "./homePageStyles";
import professorIcon from "../../draws/professor.svg";
import CircleComponents from "../../components/circles/CircleComponents";

const HomePage = () => {
  const classes = useStyles();

  return (
    <Container>
      <CircleComponents/>
      <Grid
        container
        spacing={2}
        className={classes.header}
        justify="center"
        alignItems="center"
      >
        <Grid item xs={12} sm={6}>
          <Typography
            component="h1"
            variant="h3"
            color="primary"
            className={classes.title}
          >
            Classroom Solution
          </Typography>
          <Typography
            component="p"
            color="primary"
            className={classes.paragraph}
          >
            Bienvendio a la plataforma ClassroomSolution. Nuestro objetivo es ofrecerte una herramienta que sea capaz de emular lo mejor posible un salón de clases.
          </Typography>
          <Grid container spacing={2}>
            <Grid item xs={12} md={5}>
              <Link to="/login-teachers" className={classes.link}>
                <Button
                  variant="outlined"
                  color="primary"
                  fullWidth
                >
                  Profesores
                </Button>
              </Link>
            </Grid>
            <Grid item xs={12} md={5}>
            <Link to="/login-headmasters" className={classes.link}>
                <Button
                  variant="outlined"
                  color="secondary"
                  fullWidth
                >
                  Directores
                </Button>
              </Link>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6}>
          <img src={professorIcon} className={classes.imgContainer} alt="professorLogo"/>
        </Grid>
      </Grid>
    </Container>
  );
};

export default HomePage;
