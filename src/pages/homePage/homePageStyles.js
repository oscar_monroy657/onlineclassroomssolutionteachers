import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
  header: {
    [theme.breakpoints.only('xs')]: {
      flexDirection: 'column-reverse',
    }
  },
  link : {
    textDecoration:'none'
  },
  imgContainer : {
    width: '100%',
    height: '100%',
    backgroundSize: 'cover'
  },
  title: {
    marginBottom: '2rem',
    [theme.breakpoints.only('xs')]: {
      textAlign: 'center'
    },
    [theme.breakpoints.down('md')]: {
      fontSize: '2rem',
      marginBottom: '1.2rem',
      marginTop: '1.2rem'
    }
  },
  paragraph: {
    fontSize: '1.3rem',
    marginBottom: '2rem',
    [theme.breakpoints.only('xs')]: {
      padding: '.5rem'
    },
    [theme.breakpoints.down('md')]: {
      fontSize: '1rem',
      marginBottom: '1.2rem'
    }
  },
  container: {
    height: '150px',
    overflow: 'hidden'
  }
}))