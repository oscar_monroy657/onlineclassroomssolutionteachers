import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom'
import { Grid, Container, Typography, Button, Paper, TextField, CircularProgress } from "@material-ui/core";
import accessAccount from '../../draws/access_account.svg'
import { useStyles } from "./loginHeadmasterStyles";
import CircleComponents from "../../components/circles/CircleComponents";
import { firebaseConsumer } from '../../server';
import { useSessionStateValue } from '../../session/SessionContext'
import displaySnackBar from '../../session/actions/snackBarActions';
import { signInHeadmasterAction } from '../../session/actions/sessionActions';

const LoginHeadmaster = ({firebase, history}) => {
  const classes = useStyles();
  const [{session}, dispatch] = useSessionStateValue()

  const [userPassword, setUserPassword] = useState('')
  const [userEmail, setUserEmail] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const handleOnSubmit = e => {
    e.preventDefault()
    
    if(userEmail === '' || userPassword === '') {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: 'Todos los campos son necesarios.'
      })
      return
    }

    setIsLoading(true)
    signInHeadmasterAction(dispatch, firebase, userEmail, userPassword)
    .then(res => {
      history.push('/dashboard-headmaster')
    })
    .catch(error => {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: error
      })
      setIsLoading(false)
    })
  }

  return (
    <Container>
      <CircleComponents/>
      <Grid
        container
        spacing={2}
        className={classes.container}
        alignItems="center"
        justify="space-around"
      >
        <Grid item xs={12} sm={6} md={5} className={classes.itemContainer}>
          <Paper className={classes.paper}>
            <form className={classes.form} onSubmit={handleOnSubmit}>
              <TextField
                variant="outlined"
                label="Email"
                name="userEmail"
                fullWidth
                margin="normal"
                value={userEmail}
                onChange={e => setUserEmail(e.target.value)}
                autoComplete="username"
              />
              <TextField
                variant="outlined"
                type="password"
                label="Contraseña"
                name="userPassword"
                fullWidth
                value={userPassword}
                onChange={e => setUserPassword(e.target.value)}
                autoComplete="current-password"
              />
              <div className={classes.buttonContainer}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.button}
                >
                  {isLoading ? <CircularProgress color="inherit" size={30}/> : 'Iniciar Sesión'}
                </Button>
              </div>
            </form>
            <Grid container justify="space-between">
              <Grid item xs={12} md={5} className={classes.linkContainer}>
                <Link to='/recover-password' className={classes.link}>
                  <Typography variant="body2">¿Olvidó su contraseña?</Typography>
                </Link>
              </Grid>
              <Grid item xs={12} md={6} className={classes.linkContainer}>
                <Link to='/register-headmaster' className={classes.link}>
                  <Typography variant="body2">¿No tienes cuenta? Regístrate</Typography>
                </Link>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={5} className={classes.itemContainer}>
          <img src={accessAccount} className={classes.imgContainer} alt="accessLogo"/>
        </Grid>
      </Grid>
    </Container>
  );
}
 
export default firebaseConsumer(LoginHeadmaster);