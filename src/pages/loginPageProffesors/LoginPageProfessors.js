import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom'
import { Grid, Container, Typography, Button, Paper, TextField, CircularProgress } from "@material-ui/core";
import accessAccount from '../../draws/access_account.svg'
import { useStyles } from "./loginPageProffesorsStyles";
import CircleComponents from "../../components/circles/CircleComponents";
import { firebaseConsumer } from '../../server';
import { SelectSchoolComponent } from '../../components/selectComponent/SelectComponent';
import initialUserData from '../../utils/initialUserState';
import { useSessionStateValue } from '../../session/SessionContext'
import displaySnackBar from '../../session/actions/snackBarActions';
import { signInAction } from '../../session/actions/sessionActions';
import createSchoolLocalKeys from '../../utils/createSchoolLocalKeys';

const LoginPage = ({firebase, history}) => {
  const classes = useStyles();
  const [{session}, dispatch] = useSessionStateValue()

  const [userData, setUserData] = useState(initialUserData)
  const [userPassword, setUserPassword] = useState('')
  const [schoolData, setSchoolData] = useState([])
  const [schoolSelected, setSchoolSelected] = useState({})
  const [isFirstTime, setIsFirstTime] = useState(true)
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    if(isFirstTime){
      firebase.getPublicDataFromSchools()
      .then(data => {
        const arraySchool = data.docs.map(doc => {
          return doc.data()
        })
        setSchoolData(arraySchool)
      })
      .catch(error => console.log(error))
      setIsFirstTime(false)
    }
  }, [firebase, isFirstTime])

  const handleOnChange = e => {
    setUserData({
      ...userData,
      [e.target.name]: e.target.value
    })
  }

  const handleOnSubmit = e => {
    e.preventDefault()
    
    if(userData.userEmail === '' || userData.userSchool === '' || userPassword === '') {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: 'Todos los campos son necesarios.'
      })
      return
    }
    
    createSchoolLocalKeys(schoolSelected)

    setIsLoading(true)
    signInAction(dispatch, firebase, schoolSelected, userData.userEmail, userPassword)
    .then(res => {
      history.push('/dashboard-teacher')
    })
    .catch(error => {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: error
      })
      setIsLoading(false)
    })
  }

  return (
    <Container>
      <CircleComponents/>
      <Grid
        container
        spacing={2}
        className={classes.container}
        alignItems="center"
        justify="space-around"
      >
        <Grid item xs={12} sm={6} md={5} className={classes.itemContainer}>
          <Paper className={classes.paper}>
            <form className={classes.form} onSubmit={handleOnSubmit}>
              <TextField
                variant="outlined"
                label="Email"
                name="userEmail"
                fullWidth
                margin="normal"
                value={userData.userEmail}
                onChange={handleOnChange}
                autoComplete="username"
              />
              <SelectSchoolComponent
                arrayData={schoolData}
                labelText="Establecimiento"
                selectValue={userData.userSchool}
                changeUserSchool={handleOnChange}
                changeValueState={setSchoolSelected}
              />
              <TextField
                variant="outlined"
                type="password"
                label="Contraseña"
                name="userPassword"
                fullWidth
                value={userPassword}
                onChange={e => setUserPassword(e.target.value)}
                autoComplete="current-password"
              />
              <div className={classes.buttonContainer}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.button}
                >
                  {isLoading ? <CircularProgress color="inherit" size={30}/> : 'Iniciar Sesión'}
                </Button>
              </div>
            </form>
            <Grid container justify="space-between">
              <Grid item xs={12} md={5} className={classes.linkContainer}>
                <Link to='/recover-password' className={classes.link}>
                  <Typography variant="body2">¿Olvidó su contraseña?</Typography>
                </Link>
              </Grid>
              <Grid item xs={12} md={6} className={classes.linkContainer}>
                <Link to='/register-teacher' className={classes.link}>
                  <Typography variant="body2">¿No tienes cuenta? Regístrate</Typography>
                </Link>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={5} className={classes.itemContainer}>
          <img src={accessAccount} className={classes.imgContainer} alt="accessLogo"/>
        </Grid>
      </Grid>
    </Container>
  );
}
 
export default firebaseConsumer(LoginPage);