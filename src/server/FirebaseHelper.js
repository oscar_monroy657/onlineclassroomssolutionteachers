import app from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'
import { publicSchoolsCollection, teachersCollection } from '../utils/FirestoreNamesKey'

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATA_BASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID
};

class FirebaseHelper {
  constructor() {
    app.initializeApp(firebaseConfig)
    this.db = app.firestore()
    this.auth = app.auth()
    this.storage = app.storage()

    this.storage.ref().constructor.prototype.saveDocuments = function(documents, userName, houseName) {
      let ref = this
      const path = `${userName}/housesPhotos/${houseName}`
      return Promise.all(documents.map(file => {
        return ref.child(path+'/'+file.alias).put(file).then(snapshot => ref.child(path+'/'+file.alias).getDownloadURL())
      }))
    }
  }

  isReady() {
    return new Promise(resolve => {
      this.auth.onAuthStateChanged(resolve)
    })
  }

  getPublicDataFromSchools = () => {
    return this.db.collection(publicSchoolsCollection).orderBy('schoolName').get()
  }

  getSchoolFromFirestore = (headMasterId, schoolId) => {
    return this.db.collection(headMasterId).doc(schoolId).get()
  }

  createUser = (userEmail, userPassword) => {
    return this.auth.createUserWithEmailAndPassword(userEmail, userPassword)
  }

  loginUser = (userEmail, userPassword) => {
    return this.auth.signInWithEmailAndPassword(userEmail, userPassword)
  }

  addHeadmasterToFirestore = (headmasterId, headmasterData) => {
    return this.db.collection(headmasterId).doc(headmasterId).set(headmasterData)
  }
  
  updateHeadmasterToFirestore = (headmasterId, headmasterData) => {
    return this.db.collection(headmasterId).doc(headmasterId).set(headmasterData, {merge: true})
  }

  getHeadmasterDataFromFirestore = (headmasterId) => {
    return this.db.collection(headmasterId).doc(headmasterId).get()
  }

  addTeacherToFirestore = (headMasterId, schoolId, teacherUid, teacherData) => {
    return this.db.collection(headMasterId).doc(schoolId).collection(teachersCollection).doc(teacherUid).set(teacherData)
  }

  updateTeacherToFirestore = (headMasterId, schoolId, teacherUid, teacherData) => {
    return this.db.collection(headMasterId).doc(schoolId).collection(teachersCollection).doc(teacherUid).set(teacherData, {merge: true})
  }
  
  getTeacherDataFromFirestore = (headMasterId, schoolId, teacherUid ) => {
    return this.db.collection(headMasterId).doc(schoolId).collection(teachersCollection).doc(teacherUid).get()
  }

  updateDocumentToFirestore = (collectionName, documentId, data) => {
    return this.db.collection(collectionName).doc(documentId).set(data, {merge:true})
  }


  deleteDocumentFromFirestore = (collectionName, documentId) => {
    return this.db.collection(collectionName).doc(documentId).delete()
  }

  saveHeadmasterProfilePhoto = (fileName, file, headmasterId) => {
    const path = `${headmasterId}/headmaster/${headmasterId}/profilePhoto/${fileName}`

    return this.storage.ref().child(path).put(file)
  }

  getUrlHeadmasterProfilePhoto = (fileName, headmasterId) => {
    const path = `${headmasterId}/headmaster/${headmasterId}/profilePhoto/${fileName}`

    return this.storage.ref().child(path).getDownloadURL()
  }

  saveTeacherProfilePhoto = (fileName, file, headmasterId, schoolId, userUid) => {
    const path = `${headmasterId}/${schoolId}/teachers/${userUid}/profilePhoto/${fileName}`

    return this.storage.ref().child(path).put(file)
  }

  getUrlTeacherProfilePhoto = (fileName, headmasterId, schoolId, userUid) => {
    const path = `${headmasterId}/${schoolId}/teachers/${userUid}/profilePhoto/${fileName}`

    return this.storage.ref().child(path).getDownloadURL()
  }

  saveFilesInStorage = (documents, userName, houseName) => this.storage.ref().saveDocuments(documents, userName, houseName)

  deleteFileInStorage = (fileName, userName,  houseName) => {
    const path = `${userName}/housesPhotos/${houseName}`

    return this.storage.ref().child(path+'/'+fileName).delete()
  }
}

export default FirebaseHelper