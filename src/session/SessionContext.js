import React, { createContext, useContext, useReducer } from 'react';

export const SessionStateContext = createContext()

export const SessionStateProvider = ({reducer, initialState, children}) => (
  <SessionStateContext.Provider value={useReducer(reducer, initialState)}>
    {children}
  </SessionStateContext.Provider>
)

export const useSessionStateValue = () => useContext(SessionStateContext)