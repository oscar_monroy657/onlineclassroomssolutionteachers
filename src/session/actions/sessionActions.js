export const createUserAction = ( dispatch, firebase, schoolSelected, userPassword, userPhoto, userData) => {
  const {id, headmasterId} = schoolSelected
  const photo = userPhoto.data[0];
  const photoName = userPhoto.name;
  const photoExtension = photoName.split(".").pop();

  const profilePhotoName = ("profilePhoto." + photoExtension).replace(/\s/g, "_").toLowerCase();

  return new Promise((resolve, reject) => {
    firebase.createUser(userData.userEmail, userPassword)
    .then(auth => {
      userData.userId = auth.user.uid
      firebase.addTeacherToFirestore(headmasterId, id, auth.user.uid, userData)
        .then(sucess => {
          firebase.saveTeacherProfilePhoto(profilePhotoName, photo, headmasterId, id, auth.user.uid)
            .then(success => {
              firebase.getUrlTeacherProfilePhoto(profilePhotoName, headmasterId, id, auth.user.uid)
                .then(urlFile =>{
                  userData.userPhoto = urlFile
                  firebase.updateTeacherToFirestore(headmasterId, id, auth.user.uid, userData)
                    .then(success => {
                      dispatch({
                        type: 'REGISTER_USER',
                        user: userData,
                        school: schoolSelected
                      })
                      resolve({status: true})
                    })
                    .catch(error => {
                      resolve({status: false, message: error.message})
                    })
                })
                .catch(error => {
                  resolve({status: false, message: error.message})
                })
            })
            .catch(error => {
              resolve({status: false, message: error.message})
            })
        })
        .catch(error => {
          resolve({status: false, message: error.message})
        })
    })
    .catch(error => {
      resolve({status: false, message: error.message})
    })
  })
}

export const signInAction = (dispatch, firebase, schoolSelected, userEmail, userPassword) => {
  const {headmasterId, id} = schoolSelected

  return new Promise((resolve, reject) => {
    firebase.loginUser(userEmail, userPassword)
    .then(auth => {
      firebase.getTeacherDataFromFirestore(headmasterId, id, auth.user.uid)
        .then(doc => {
          dispatch({
            type: 'REGISTER_USER',
            user: doc.data(),
            school: schoolSelected
          })
          resolve()
        })
        .catch(error => {
          reject(error.message)
        })
    })
    .catch(error => {
      reject(error.message)
    })
  })
}

export const createHeadmasterAction = ( dispatch, firebase, userPassword, userPhoto, userData) => {
  const photo = userPhoto.data[0];
  const photoName = userPhoto.name;
  const photoExtension = photoName.split(".").pop();

  const profilePhotoName = ("profilePhoto." + photoExtension).replace(/\s/g, "_").toLowerCase();

  return new Promise((resolve, reject) => {
    firebase.createUser(userData.userEmail, userPassword)
    .then(auth => {
      userData.userId = auth.user.uid
      firebase.addHeadmasterToFirestore(auth.user.uid, userData)
        .then(sucess => {
          firebase.saveHeadmasterProfilePhoto(profilePhotoName, photo, auth.user.uid)
            .then(success => {
              firebase.getUrlHeadmasterProfilePhoto(profilePhotoName, auth.user.uid)
                .then(urlFile =>{
                  userData.userPhoto = urlFile
                  firebase.updateHeadmasterToFirestore(auth.user.uid, userData)
                    .then(success => {
                      dispatch({
                        type: 'REGISTER_USER',
                        user: userData,
                        school: {}
                      })
                      resolve({status: true})
                    })
                    .catch(error => {
                      resolve({status: false, message: error.message})
                    })
                })
                .catch(error => {
                  resolve({status: false, message: error.message})
                })
            })
            .catch(error => {
              resolve({status: false, message: error.message})
            })
        })
        .catch(error => {
          resolve({status: false, message: error.message})
        })
    })
    .catch(error => {
      resolve({status: false, message: error.message})
    })
  })
}

export const signInHeadmasterAction = (dispatch, firebase, userEmail, userPassword) => {
  let headmasterData = {}

  return new Promise((resolve, reject) => {
    firebase.loginUser(userEmail, userPassword)
    .then(auth => {
      firebase.getHeadmasterDataFromFirestore(auth.user.uid)
        .then(doc => {
          headmasterData = doc.data()
          dispatch({
            type: 'REGISTER_USER',
            user: doc.data(),
            school: {}
          })
          resolve()
        })
        .catch(error => {
          reject(error.message)
        })
    })
    .catch(error => {
      reject(error.message)
    })
  })
}

export const signOutAction = (dispatch, firebase) => {
  return new Promise((resolve, reject) => {
    firebase.auth.signOut()
      .then(sucess => {
        dispatch({
          type: 'UNREGISTER_USER'
        })
        resolve()
      })
      .catch(error => {
        reject(error.message)
      })
  })

}