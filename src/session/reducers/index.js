import sessionReducer from './sessionReducer'
import snackBarReducer from './snackBarReducer'

export const mainReducer = ({session, openSnackBar}, action) => {
  return {
    openSnackBar : snackBarReducer(openSnackBar, action),
    session: sessionReducer(session, action)
  }
}