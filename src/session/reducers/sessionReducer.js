import initialUserData from "../../utils/initialUserState"

const initialState = {
  user: initialUserData ,
  school: {},
  isAuthenticated: false
}

const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'REGISTER_USER':
      return {
        ...state,
        user: action.user,
        school: action.school,
        isAuthenticated: true
      }
    case 'UNREGISTER_USER':
      return {
        ...state,
        user: initialUserData,
        school: {},
        isAuthenticated: false
      }
  
    default:
      return state
  }
}

export default sessionReducer