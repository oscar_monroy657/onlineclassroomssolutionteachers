const createSchoolLocalKeys = (schoolSelected) => {
  const schoolKeys = {
    schoolId: schoolSelected.id,
    headmasterId: schoolSelected.headmasterId
  }

  localStorage.setItem('schoolKeys', JSON.stringify(schoolKeys))
}

export default createSchoolLocalKeys