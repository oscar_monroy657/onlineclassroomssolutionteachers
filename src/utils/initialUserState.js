const initialUserData = {
  userId: '',
  userName: '',
  userLastName: '',
  userEmail: '',
  userSchool: '',
  userSubjects: [],
  userPhoto: '',
}

export default initialUserData